# messenger-gtk

A GTK based GUI for the Messenger service of GNUnet.

## (Planned) Dependencies

 - [gnunet](https://git.gnunet.org/gnunet.git/)
 - [libgnunetchat](https://gitlab.com/gnunet-messenger/libgnunet-chat)
 - [gtk3](https://gitlab.gnome.org/GNOME/gtk)
 - [libhandy](https://gitlab.gnome.org/GNOME/libhandy/)
 - [libnotify](https://gitlab.gnome.org/GNOME/libnotify)
